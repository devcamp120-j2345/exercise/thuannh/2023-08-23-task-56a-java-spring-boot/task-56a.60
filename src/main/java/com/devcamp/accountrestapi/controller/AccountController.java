package com.devcamp.accountrestapi.controller;

import java.util.ArrayList;

import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import com.devcamp.accountrestapi.Account;

@RestController
public class AccountController {
    @CrossOrigin
    @GetMapping("/accounts")
    public ArrayList<Account> getListAccount(){
        Account account1 = new Account("1", "Thuan", 100000);
        Account account2 = new Account("2", "Toan", 500000);
        Account account3 = new Account("3", "Tuan", 700000);

        System.out.println(account1);
        System.out.println(account2);
        System.out.println(account3);

        ArrayList<Account> accounts = new ArrayList<>();

        accounts.add(account1);
        accounts.add(account2);
        accounts.add(account3);

        return accounts;
    }
}
